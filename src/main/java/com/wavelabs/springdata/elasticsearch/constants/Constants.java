package com.wavelabs.springdata.elasticsearch.constants;

public class Constants {

	public static final String FAILED = "Failed";
	public static final String STUDENT_DATA_ADDED_SUCCESFULLY = "Student Data Added Successfully";
	public static final String SUCCESS = "Success";
	public static final String ALL_STUDENTS_DATA_FETCHED_SUCCESSFULLY = "All students data fetched successfully";
	public static final String STUDENT_NOT_FOUND_WITH_ID = "Student data not found with Id : ";
	public static final String STUDENT_DATA_UPDATED_SUCCESFULLY = "Student Data Updated Succesfully";
	public static final String STUDENT_NOT_FOUND_WITH_SEARCHED_NAME = "Student not found with searched name";
	public static final String LIST_ISEMPTY = "List is Empty :";

}
