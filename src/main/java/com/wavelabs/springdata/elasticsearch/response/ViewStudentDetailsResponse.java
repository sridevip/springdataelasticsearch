package com.wavelabs.springdata.elasticsearch.response;

import java.util.List;

import com.wavelabs.springdata.elasticsearch.entity.Student;

public class ViewStudentDetailsResponse extends Response{
	private String status;
	private List<Student> data;
	private int total;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<Student> getData() {
		return data;
	}
	public void setData(List<Student> data) {
		this.data = data;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	

}
