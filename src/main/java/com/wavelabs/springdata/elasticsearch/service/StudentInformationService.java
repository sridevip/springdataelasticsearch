package com.wavelabs.springdata.elasticsearch.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.springdata.elasticsearch.constants.Constants;
import com.wavelabs.springdata.elasticsearch.entity.Student;
import com.wavelabs.springdata.elasticsearch.exception.DataNotFoundException;
import com.wavelabs.springdata.elasticsearch.repository.StudentRepository;
import com.wavelabs.springdata.elasticsearch.request.EditStudentDetailsRequest;
import com.wavelabs.springdata.elasticsearch.request.StudentDetailsRequest;
import com.wavelabs.springdata.elasticsearch.response.Response;
import com.wavelabs.springdata.elasticsearch.response.ViewStudentDetailsResponse;

@Service
public class StudentInformationService {

	@Autowired
	StudentRepository studentRepository;

	public Response addStudentInformation(StudentDetailsRequest studentDetailsRequest) {
		Student student = new Student();
		student.setName(studentDetailsRequest.getName());
		student.setAddress1(studentDetailsRequest.getAddress1());
		student.setAddress2(studentDetailsRequest.getAddress2());
		student.setBranch(studentDetailsRequest.getBranch());
		student.setCity(studentDetailsRequest.getCity());
		student.setEmailId(studentDetailsRequest.getEmailId());
		student.setFatherName(studentDetailsRequest.getFatherName());
		student.setPhoneNumber(studentDetailsRequest.getPhoneNumber());
		student.setRollNumber(studentDetailsRequest.getRollNumber());
		student.setState(studentDetailsRequest.getState());
		studentRepository.save(student);
		Response response = new Response();
		response.setId(student.getId());
		response.setStatus(Constants.SUCCESS);
		response.setMessage(Constants.STUDENT_DATA_ADDED_SUCCESFULLY);
		return response;
	}

	public ViewStudentDetailsResponse viewAllStudentDetails() {
		List<Student> studentsList = new ArrayList<>();
		Iterable<Student> students = studentRepository.findAll();
		students.forEach(studentsList::add);
		if (studentsList.isEmpty()) {
			throw new DataNotFoundException(Constants.LIST_ISEMPTY + studentsList.size());
		}
		ViewStudentDetailsResponse viewStudentDetailsResponse = new ViewStudentDetailsResponse();
		viewStudentDetailsResponse.setData(studentsList);
		viewStudentDetailsResponse.setMessage(Constants.ALL_STUDENTS_DATA_FETCHED_SUCCESSFULLY);
		viewStudentDetailsResponse.setStatus(Constants.SUCCESS);
		viewStudentDetailsResponse.setTotal(studentsList.size());
		return viewStudentDetailsResponse;
	}

	public Response editStudentInformation(EditStudentDetailsRequest editStudentDetailsRequest) {
		Optional<Student> student = studentRepository.findById(editStudentDetailsRequest.getStudentId());

		if (!student.isPresent()) {
			throw new DataNotFoundException(
					Constants.STUDENT_NOT_FOUND_WITH_ID + editStudentDetailsRequest.getStudentId());
		}
		Student std = student.get();
		std.setAddress1(editStudentDetailsRequest.getAddress1());
		std.setAddress2(editStudentDetailsRequest.getAddress2());
		std.setCity(editStudentDetailsRequest.getCity());
		std.setEmailId(editStudentDetailsRequest.getEmailId());
		std.setState(editStudentDetailsRequest.getState());
		studentRepository.save(std);
		Response response = new Response();
		response.setMessage(Constants.STUDENT_DATA_UPDATED_SUCCESFULLY);
		return response;
	}

	public List<Student> searchStudentInformationByName(String studentName) {
		List<Student> studentList = studentRepository.findByName(studentName);
		if (studentList.isEmpty()) {
			throw new DataNotFoundException(Constants.STUDENT_NOT_FOUND_WITH_SEARCHED_NAME);
		}
		return studentList;
	}

}
