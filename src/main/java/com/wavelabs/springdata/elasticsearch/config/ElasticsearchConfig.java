package com.wavelabs.springdata.elasticsearch.config;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = "com.wavelabs.springdata.elasticsearch.repository")
public class ElasticsearchConfig {
	@Value("${elasticsearch.host}")
	private String esHost;

	@Value("${elasticsearch.port}")
	private int esPort;

//	@Value("${elasticsearch.clustername}")
//	private String esClusterName;

	@Bean
	public RestHighLevelClient client() throws Exception {
//		Settings esSettings = Settings.settingsBuilder().put("cluster.name", esClusterName).build();
//		return TransportClient.builder().settings(esSettings).build()
//				.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(esHost), esPort));


		final ClientConfiguration clientConfiguration = ClientConfiguration.builder()
						.connectedTo(esHost+":"+esPort)
						.build();

		return RestClients.create(clientConfiguration).rest();
	}

//	@Bean
//	public ElasticsearchOperations elasticsearchTemplate() throws Exception {
//		return new ElasticsearchTemplate(client());
//	}
}