package com.wavelabs.springdata.elasticsearch.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.wavelabs.springdata.elasticsearch.entity.Student;

@Repository
public interface StudentRepository extends ElasticsearchRepository<Student, Long> {

	Optional<Student> findById(Long studentId);

	List<Student> findByName(String studentName);

}
