package com.wavelabs.springdata.elasticsearch.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.wavelabs.springdata.elasticsearch.constants.Constants;

@ControllerAdvice
@RestController
public class CustomizedExpectionHandler extends ResponseEntityExceptionHandler{
	
	/**
	 * @param ex
	 * @param request
	 * @return
	 */	
	@ExceptionHandler(DataNotFoundException.class)
	public final ResponseEntity<ErrorResponse> handleDataNotFoundException(DataNotFoundException exception,
			WebRequest request) {
		ErrorResponse errorResponse = new ErrorResponse(Constants.FAILED,exception.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
	}
}
