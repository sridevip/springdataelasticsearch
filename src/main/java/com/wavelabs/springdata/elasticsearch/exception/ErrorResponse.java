package com.wavelabs.springdata.elasticsearch.exception;

import java.time.Instant;

public class ErrorResponse {
	
	private String status;
	private Instant timestamp = Instant.now();
	private String error;
	private String path;

	public ErrorResponse(String status,String error, String path) {
		this.status=status;
		this.error = error;
		this.path = path;
	}
	
	
	public String getStatus() {
		return status;
	}

	public Instant getTimestamp() {
		return timestamp;
	}

	public String getError() {
		return error;
	}

	public String getPath() {
		return path;
	}

	
}
