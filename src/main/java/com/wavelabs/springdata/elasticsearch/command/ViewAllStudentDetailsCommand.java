package com.wavelabs.springdata.elasticsearch.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.wavelabs.springdata.elasticsearch.response.ViewStudentDetailsResponse;
import com.wavelabs.springdata.elasticsearch.service.StudentInformationService;

@Service
public class ViewAllStudentDetailsCommand implements ViewStudentsCommand<ResponseEntity<ViewStudentDetailsResponse>>{
	
	@Autowired
	StudentInformationService studentInformationService;

	
	@Override
	public ResponseEntity<ViewStudentDetailsResponse> execute() {
		return ResponseEntity.status(HttpStatus.OK).body(studentInformationService.viewAllStudentDetails());
	}
	
}
