package com.wavelabs.springdata.elasticsearch.command;

public interface ViewStudentsCommand <T>{
	public T execute();
}
