package com.wavelabs.springdata.elasticsearch.command;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.springdata.elasticsearch.entity.Student;
import com.wavelabs.springdata.elasticsearch.service.StudentInformationService;

@Service
public class SearchStudentCommand implements Command<String, List<Student>>{

	@Autowired
	StudentInformationService studentInformationService;
	
	@Override
	public List<Student> execute(String studentName) {
		return studentInformationService.searchStudentInformationByName(studentName);
	}

}
