package com.wavelabs.springdata.elasticsearch.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.springdata.elasticsearch.request.EditStudentDetailsRequest;
import com.wavelabs.springdata.elasticsearch.response.Response;
import com.wavelabs.springdata.elasticsearch.service.StudentInformationService;

@Service
public class EditStudentCommand implements Command<EditStudentDetailsRequest, Response>{
	
	@Autowired
	StudentInformationService studentInformationService;

	@Override
	public Response execute(EditStudentDetailsRequest request) {
		return studentInformationService.editStudentInformation(request);
	}

}
