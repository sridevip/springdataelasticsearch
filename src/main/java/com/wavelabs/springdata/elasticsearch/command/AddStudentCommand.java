package com.wavelabs.springdata.elasticsearch.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.wavelabs.springdata.elasticsearch.exception.DataNotFoundException;
import com.wavelabs.springdata.elasticsearch.request.StudentDetailsRequest;
import com.wavelabs.springdata.elasticsearch.response.Response;
import com.wavelabs.springdata.elasticsearch.service.StudentInformationService;

@Service
public class AddStudentCommand implements Command<StudentDetailsRequest, Response>{
	
	@Autowired
	StudentInformationService studentInformationService;
	
	@Override
	public Response execute(StudentDetailsRequest request) {
		
		if(StringUtils.isEmpty(request.getAddress1())) {
			throw new DataNotFoundException("Address field is Mandatory");
		}
		
		if(StringUtils.isEmpty(request.getCity())) {
			throw new DataNotFoundException("City field is Mandatory");
		}
		
		if(StringUtils.isEmpty(request.getEmailId())) {
			throw new DataNotFoundException("Email field is Mandatory");
		}
		
		if(StringUtils.isEmpty(request.getName())) {
			throw new DataNotFoundException("Name field is Mandatory");
		}
		
		if(StringUtils.isEmpty(request.getPhoneNumber())) {
			throw new DataNotFoundException("PhoneNumber field is Mandatory");
		}
		return studentInformationService.addStudentInformation(request);
	}

}
